# flatlib

Author: Tom DERUDDER  
Version: `2.1.0`

---

> css library inspired by atomic design

## Outbuildings

- Node.js
- npm
- serve

## Install

```bash
git clone 'git@gitlab.com:tomderudder/flatlib.git' flatlib
cd flatlib
yarn
```

## Usage

### Serve

```bash
serve dist
```

## Build

```bash
yarn build
```

## Contributing

Please refer to our [Contribution Guide](contributing.md)

## Useful links

- [circular progress](http://jsfiddle.net/RrAVS/)
- https://medium.com/@audreyhacq/l-atomic-design-une-m%C3%A9thode-de-co-creation-prometteuse-bd9d5fc2b2ad
- https://putaindecode.io/fr/articles/css/bem/

## Logo

![logo](misc/logo.png)
