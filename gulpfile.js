'use strict'

// modules
require('dotenv').config()
const { parallel, series, src, dest, watch } = require('gulp')
const stylus = require('gulp-stylus')
const concat = require('gulp-concat')
const autoprefixer = require('gulp-autoprefixer')
const cssnano = require('gulp-cssnano')
const htmlmin = require('gulp-htmlmin')
const iconfont = require('gulp-iconfont')
const iconfontCss = require('gulp-iconfont-css')

// constants
const env = process.env.NODE_ENV
const ICON_FONT_NAME = 'icons'
const SRC_STYLUS = './src/main.styl' // ./lib/**/*.styl
const SRC_HTML = './pages/*.html'
const SRC_ICONS = './src/icons/**/*.svg'
const DEST = './dist'
const DEST_FILENAME = 'flatlib.min.css'

// stylus
function stylusToCss() {
  return src(SRC_STYLUS)
    .pipe(stylus({ linenos: env === 'development' }))
    .pipe(concat(DEST_FILENAME))
    .pipe(autoprefixer())
    .pipe(cssnano())
    .pipe(dest(DEST))
}

// icons font
function iconToFont() {
  return src(['src/icons/*.svg'])
    .pipe(
      iconfontCss({
        fontName: ICON_FONT_NAME,
        targetPath: '../flatlib-icons.css',
        fontPath: './fonts/'
      })
    )
    .pipe(
      iconfont({
        fontName: ICON_FONT_NAME,
        formats: ['ttf', 'eot', 'woff', 'woff2'],
        appendCodepoints: true,
        prependUnicode: true,
        normalize: true,
        fontHeight: 1000,
        centerHorizontally: true,
        timestamp: Math.round(Date.now() / 1000)
      })
    )
    .pipe(dest(DEST + '/fonts'))
}
function minifyIconFontCss() {
  return src(DEST + '/flatlib-icons.css')
    .pipe(autoprefixer())
    .pipe(concat('/flatlib-icons.min.css'))
    .pipe(cssnano())
    .pipe(dest(DEST))
}

// html
function copyHtml() {
  return src(SRC_HTML)
    .pipe(htmlmin({ collapseWhitespace: true, removeComments: true }))
    .pipe(dest(DEST))
}

function watchers() {
  watch([SRC_STYLUS, 'vars.styl', './src/**/*.styl'], series(stylusToCss))
  watch(SRC_HTML, series(copyHtml))
  watch(SRC_ICONS, series(iconToFont, minifyIconFontCss))
}

exports.default = exports.build = parallel(
  series(iconToFont, minifyIconFontCss),
  stylusToCss,
  copyHtml
)
exports.watch = watchers
